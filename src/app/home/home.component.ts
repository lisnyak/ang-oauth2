import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor(private oauthService: OAuthService) {
  }

  login() {
    console.log("initImplicitFlow");
    this.oauthService.initImplicitFlow();
  }

  logout() {
    this.oauthService.logOut();
  }

  get givenName() {
    let claims = this.oauthService.getAccessToken();
    if (!claims) {
      return null;
    }
    return JSON.parse(atob(claims.split(".")[1]));
  }
  ngOnInit() {
    if (!this.oauthService.hasValidAccessToken()) {
      this.oauthService.initImplicitFlow();
    }

  }
}
