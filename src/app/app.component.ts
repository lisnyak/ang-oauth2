import { Component } from '@angular/core';

import { OAuthService, JwksValidationHandler, AuthConfig } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private oauthService: OAuthService) {

    let Config: AuthConfig = {
      redirectUri: window.location.origin,
      clientId: 'test',
      scope: '',
      loginUrl: 'http://oauth.cruisemgmt.itnavigator.pp.ua/oauth2/authorize',
      nonceStateSeparator: ";",
      requireHttps: false,
      responseType: 'token',
      oidc: false,
      requestAccessToken: true
    }
    this.oauthService.configure(Config);
    this.oauthService.tryLogin({
      onTokenReceived: context => {
        console.log("logged in");
        console.log(context);
      }
    });


  }

}
